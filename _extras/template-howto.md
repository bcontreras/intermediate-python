---
layout: page
title: How to use the lesson template
permalink: /template-howto/
---

## Notes on how to use this lesson template

See the [Lesson Example][lesson-example]
and [The Carpentries Curriculum Development Handbook][cdh]
for full details.
Below should be all the things you need to know right now...

### Creating pages

- Write material with [Markdown][markdown-cheatsheet]
  - markdown files will be rendered as HTML pages and included in the built site
- `.md` files in the `_episodes` folder will be added to the Episodes dropdown, page navigation, etc
- Markdown files must include _front matter_: metadata specified in a YAML header bounded by `---`
- At minimum, this must include a `title` field

~~~
---
title: The Title of the Section
---
~~~
{: .source}

- but really your episodes (lesson sections) should include:
  - an estimate of time required for teaching & exercises
  - main questions answered in the section
  - learning objectives
  - key points to summarise what's covered in the section (these end are added at the end of the lession section)
- as an example, below is the front matter for this page

~~~
---
title: Syntax Elements & Powerful Functions
teaching: 20
exercises: 10
questions:
- "What elements of Python syntax might I see in other people's code?"
- "How can I use these additional features of Python to take my code to the next level?"
- "What built-in functions and standard library modules are recommended to improve my code?"
objectives:
- "write comprehensions to improve code readability and efficiency."
- "call functions designed to make common tasks easier and faster."
- "recognise all elements of modern Python syntax and explain their purpose."
keypoints:
- "Use comprehensions to efficiently create new iterables with fewer lines of code."
- "Sets can be extremely useful when comparing collections of objects, and create significantly speed up your code."
- "The `itertools` module includes many helpful functions for working with iterables."
- "A decorator is a function that does something to the output of another function."
---
~~~
{: .source}

## Code blocks

code snippets written like this

{% raw %}
    ~~~
    print(weight_kg)
    ~~~
    {: .language-python}
    ~~~
    60.0
    ~~~
    {: .output}
{% endraw %}

will produce formatted blocks like this:

~~~
print(weight_kg)
~~~
{: .language-python}
~~~
60.0
~~~
{: .output}

## Special blockquotes

- The lesson template also includes a range of styled boxes
  - examples for exercises and callouts below
  - see [this section][lesson-example-blockquotes] of The Carpentries Lesson Example for the full list

A callout block written like this

~~~
> ## Callout block example
>
> Write callout blocks as blockquotes,
> with a styling tag (techincal term is a _class identifier_) at the end.
>
> ~~~
> # you can still include code blocks in the callout
> weight_lb = 2.2 * weight_kg
> print(weight_kg_text, weight_kg, 'and in pounds:', weight_lb)
> ~~~
> {: .language-python}
>
> Use callouts for asides and comments -
> anything that provides additional detail to the core of your material
{: .callout}
~~~
{: .source}

will be rendered like this:

> ## Callout block example
>
> Write callout blocks as blockquotes,
> with a styling tag (techincal term is a _class identifier_) at the end.
>
> ~~~
> # you can still include code blocks in the callout
> weight_lb = 2.2 * weight_kg
> print(weight_kg_text, weight_kg, 'and in pounds:', weight_lb)
> ~~~
> {: .language-python}
>
> Use callouts for asides and comments -
> anything that provides additional detail to the core of your material
{: .callout}

Similarly, exercises written like this

~~~
> ## Sorting Out References
>
> What does the following program print out?
>
> ~~~
> first, second = 'Grace', 'Hopper'
> third, fourth = second, first
> print(third, fourth)
> ~~~
> {: .language-python}
>
> > ## Solution
> >
> > This text will only be visible if the solution is expanded
> > ~~~
> > Hopper Grace
> > ~~~
> > {: .output}
> {: .solution}
{: .challenge}
~~~
{: .source}

will be rendered like this (note the expandable box containing the solution):

> ## Sorting Out References
>
> What does the following program print out?
>
> ~~~
> first, second = 'Grace', 'Hopper'
> third, fourth = second, first
> print(third, fourth)
> ~~~
> {: .language-python}
>
> > ## Solution
> >
> > This text will only be visible if the solution is expanded
> > ~~~
> > Hopper Grace
> > ~~~
> > {: .output}
> {: .solution}
{: .challenge}

## Shared link references

- Lastly, the last line in every `.md` file for each page should be

{% raw %}
`{% include links.md %}`
{% endraw %}

- This allows us to share link references across the entire site, which makes the links much more maintainable.
  - link URLs should be put in the `_includes/links.md` file (ideally, arranged alphabetically by reference)
  - you can then write Markdown links "reference-style" i.e. `[link text to be displayed][reference-id]`, with `[reference-id]: https://link.to.page` in `_includes/links.md`

{% include links.md %}
