---
title: Plotting Data
teaching: 20
exercises: 10
questions:
- "How can I create publication-ready figures with Python?"
objectives:
- "plot data in a Matplotlib figure."
- "create multi-panelled figures."
- "export figures in a variety of image formats."
- "use interactive features of Jupyter to make it easier to fine-tune a plot."
keypoints:
- "Matplotlib is a powerful plotting library for Python."
- "It can also be annoyingly fiddly. Jupyter can help with this."
---

Coming soon..!

{% include links.md %}
