import argparse

parser = argparse.ArgumentParser()
parser.add_argument("input_file",
                    help="path to an input file for processing")
parser.add_argument("number1", help="the first number")
parser.add_argument("number2", help="the second number")

args = parser.parse_args()

print(f"input_file provided was {args.input_file}")
print(args.number1)
print(args.number2)
