import argparse

parser=argparse.ArgumentParser()
parser.add_argument("input_file",
                    help="path to an input file for processing")
parser.add_argument("numbers", type=int, nargs="+",
                    help="integers to be summed")
parser.add_argument("--language", "-l",
                    choices=["en", "de", "es", "pt"], default="en",
                    help="language of logging messages")
parser.add_argument("--output", "-o",
                    help="path to an output file (default: print to STDOUT)")
group = parser.add_mutually_exclusive_group()
group.add_argument("--verbose", "-v", action="store_true")
group.add_argument("--silent", "-q", action="store_true")

args = parser.parse_args()

if args.verbose:
    print("Starting...")

if not args.silent:
    logging_messages = {
        "en": "input file:\t{}",
        "de": "Eingabedatei:\t{}",
        "es": "nombre de archivo de entrada:\t{}",
        "pt": "nome do arquivo de entrada:\t{}",
    }

    message = logging_messages[args.language].format(args.input_file)
    print(message)

if args.verbose:
    print("Calculating result...")
result = sum(args.numbers)
if args.output:
    if args.verbose:
        print("Writing result to file...")
    with open(args.output, "w") as outfh:
        outfh.write(f"{result}\n")
else:
    print(result)

if args.verbose:
    print("Finishing...")
