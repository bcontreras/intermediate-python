import argparse

parser=argparse.ArgumentParser()
parser.add_argument("input_file",
                    help="path to an input file for processing")
parser.add_argument("numbers", type=int, nargs="+",
                    help="integers to be summed")
parser.add_argument("--language", "-l",
                    choices=["en", "de", "es", "pt"], default="en",
                    help="language of logging messages")
parser.add_argument("--output", "-o",
                    help="path to an output file (default: print to STDOUT)")

args = parser.parse_args()

logging_messages = {
    "en": "input file:\t{}",
    "de": "Eingabedatei:\t{}",
    "es": "nombre de archivo de entrada:\t{}",
    "pt": "nome do arquivo de entrada:\t{}",
}

message = logging_messages[args.language].format(args.input_file)
print(message)

result = sum(args.numbers)
if args.output:
    with open(args.output, "w") as outfh:
        outfh.write(f"{result}\n")
else:
    print(result)
