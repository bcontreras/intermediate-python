def find_anagrams(words,ignore_case = True) :
    anagrams = []
    if ignore_case == True:
        charsets = [ set(w.lower()) for w in words ]
        return [w for w in words if charsets.count(set(w.lower())) > 1]
    else:
        charsets = [ set(w) for w in words ]
        return [w for w in words if charsets.count(set(w)) > 1]

test_words = "back to the time when trouble was not always on our minds No mite item".split()

print(find_anagrams(test_words, ignore_case=False))
