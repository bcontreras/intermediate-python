import argparse

parser=argparse.ArgumentParser()
parser.add_argument("input_file",
                    help="path to an input file for processing")
parser.add_argument("number1", type=int, help="the first number")
parser.add_argument("number2", type=int, help="the second number")
parser.add_argument("--output", "-o",
                    help="path to an output file (default: print to STDOUT)")
args = parser.parse_args()

print(f"input_file provided was {args.input_file}")

result = args.number1 + args.number2
if args.output:
    with open(args.output, "w") as outfh:
        outfh.write(f"{result}\n")
else:
    print(result)
